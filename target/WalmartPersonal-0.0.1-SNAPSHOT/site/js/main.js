function showPopUp(id)
{
  document.getElementById(id).style.display='block';
  return false;
}

function hidePopUp(id)
{
  document.getElementById(id).style.display='';
  return false;
}

function addToList(list, name, formName)
{
  var L = document.getElementById(list);
  var item = document.createElement('li');
  var anchor = document.createElement('a');
  var spanText = document.createElement('span');

  var text = document.createTextNode(document.getElementsByName(name)[0].value);

  spanText.appendChild(text);
  anchor.appendChild(spanText);
  item.appendChild(anchor);
  L.appendChild(item);
  hidePopUp(formName);
}

function paggination() {
var w = 0;
var widthPagination = document.getElementById('billboard').clientWidth;
var sliderPagination = document.getElementById('slideWrapper');
(document.getElementById('billboard')).addEventListener("load", setInterval(pageTranslate,3000));

function pageTranslate()
{
  w = (w > 3*widthPagination)?0:(w+widthPagination);
  sliderPagination.scrollLeft = w;
  //var wid = w.toString()+"px";
  //sliderPagination.style.transform = 'translate3d('+w.toString()+'px ,0px ,0px)';
}
};
