var user = {
  name:"",
  kidAge:""
};

function showPopUp(id)
{
  document.getElementById(id).style.display='block';
  if (id === 'assistantPopup')
    document.getElementById('assistantSideBar').style.display='';
  return false;
}

function hidePopUp(id)
{
  document.getElementById(id).style.display='';
  if (id === 'assistantPopup')
    document.getElementById('assistantSideBar').style.display='block';
  return false;
}

function addToList(list, name, formName, texty, selc)
{
  var L = document.getElementById(list);
  var item = document.createElement('li');
  if (texty !== 'N' || selc === 'sel')
    item.style.backgroundColor = "rgb(54,153,219)";
  var anchor = document.createElement('a');
  if (texty !== 'N' || selc === 'sel')
    anchor.style.color = "white";
  var spanText = document.createElement('span');
  var text = null;
  if (texty === 'N')
    text = document.createTextNode(name);
  else
    text = document.createTextNode(document.getElementsByName(name)[0].value);

  spanText.appendChild(text);
  anchor.appendChild(spanText);
  item.appendChild(anchor);
  L.appendChild(item);

  var bread = document.getElementById('breadcrumbs');
  var str = bread.innerHTML;
  if (texty !== 'N')
    str += '<span> > </span><a><span>'+document.getElementsByName(name)[0].value+'</a><span>';
  bread.innerHTML = str;
  hidePopUp(formName);
}

function assistLoad() {
var cont = document.getElementById("assistantContent");
var w = 0;
(document.getElementById('assistantContent')).addEventListener("load", setInterval(pageTranslate,5000));

function pageTranslate()
{
  if (ast != null && w <= ast.length)
  {
  var innerC = cont.innerHTML;
  innerC = ast[w] + innerC;
  cont.innerHTML = innerC;
}
}
}

var ast = null;

var flag = true;

function addContent(param, age)
{
  if (!flag)
    return;
  var cont = document.getElementById("assistantContent");
  var innerC = '';
  if (param === "babyMilk")
  {
  innerC = "<p>New recommendations from experts:<br>Pedasure is the best milk supplement. <a>Study here</a><br>Do Read these :<br>"+
  "<a href='http://www.webmd.boots.com/children/baby/guide/breast-feeding-diet'>Breast feeding guide</a><br>"+
  "<a href='http://www.webmd.boots.com/children/baby/guide/breast-feeding-diet'>How to feed a baby</a><br>"+
  "<a href='http://incredibleinfant.com/sleeping/newborn-schedule/'>Baby Hygiene</a><br>"+
  "<a href='http://incredibleinfant.com/sleeping/newborn-schedule/'>Keeping the baby warm</a><br></p>";
  cont.innerHTML += innerC;
  }
  if (param === 'baby')
  {
    cont.innerHTML = "";
    if(age === '3')
    {
      innerC = '<p>Your sister Jane just joined the assist program for Anna, who is a new born.<br>Being a more recent mother,'+
      ' it would be helpful for her to connect with you.<br><a>Connect</a></p><div>Hi Lisa, we are stocked up well for '+
      'Mary:<br><a>Tissue Wipes</a><br><span>Bought 5 days ago, stock till next 5 days</span><br><a>'+
      'Cotton Clothes</a><br><span>7, well stocked</span><br><a>Diapers</a><br><span>Bought 5 days ago, stock'+
      ' till next 5 days.</span><br><a>Mitten Gloves</a><br><span>4, well stocked</span><br><a>Bibs</a><br>'+
      '<span>4, well stocked</span><br><a>Socks</a><br><span>6 pairs, well stocked</span><br></div>';
    }
    else if(age === '18')
    {
      innerC = '<p>Check offers for Mary<br><a>Picture book of animals</a><br><a>Toys</a><br><a>Clothes</a><br>'+
      '<a>Music instruments for toddlers</a><br></p><p>Time for next vaccine is coming up<br><a>Check Vaccine schedule</a><br></p>'+
      '<p>Must Reads:<br><a>Cognitive development</a> milestones 18 months</p>';
    }
    else if(age === '24')
    {
      innerC = '<p>Check the latest Toys, which helps in:<br><a>Fine motor development</a><br><a>Cognitive development</a><br></p>'+
      '<p>New arrivals in 2-3 year children\'s <a>clothes</a></p>';
    }

    cont.innerHTML += innerC;
  }
  flag = false;
}

function logReg(name) {
  var url = window.location.href;
  var last = url.lastIndexOf('/');
  var red = url.substring(0,last);
  var inname = (document.getElementsByName(name)[0]).value;
  var redirect = null;
  if (name === 'maryAge')
  {
    redirect = red + '/user_home.html?user=Lisa&kidAge='+inname;
  }
  else
  {
    redirect = red + '/user_home.html?user='+inname;
  }
  window.open(redirect,'_self',false);
  return false;
}

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return '';
}

function userContentChange(entity)
{
  user["name"] = getQueryVariable('user');
  user["kidAge"] = getQueryVariable('kidAge');
  if (document.getElementById('welcomeUser') != null)
    document.getElementById('welcomeUser').innerHTML = "Welcome " + user["name"];
  if (user["name"] === "Lisa")
  {
    if (user["kidAge"] != '')
      addToList('peopleList', 'Mary (Child)', 'addPeopleForm', 'N', 'sel');
    else {
      addToList('peopleList', 'Mary (Child)', 'addPeopleForm', 'N');
    }
    addToList('peopleList', 'George (Husband)', 'addPeopleForm', 'N');
    addToList('peopleList', 'Martha (Mother)', 'addPeopleForm', 'N');
    addToList('groupList', 'Camping', 'addGroupForm', 'N');
    addToList('groupList', 'Beach Party', 'addGroupForm', 'N');
    hidePopUp('assistantPopup');
    var anchor = (document.getElementById('peopleList')).children[1];
    anchor.onclick = "randomContent('baby0')";
    if (user["kidAge"] != '')
      addContent('baby', user["kidAge"]);
    else {
      addContent('baby', '3');
    }
  }
  return entity;
}

window.addEventListener("load", randomContent);

function randomContent(entity)
{
  entity = userContentChange(entity);
  var gridLayout = document.getElementById('categoryGrid');
  if (gridLayout == null)
    return;
  var xmlReq = new XMLHttpRequest();
  var url = null;
  if (user["kidAge"] === "3")
    entity = 'baby0';
  else if (user["kidAge"] === "18")
    entity = 'baby18';
  else if ((user["kidAge"] === "24"))
    entity = 'babyGirl';
  if (entity === '' || typeof entity !== 'string')
    entity = 'xyz';
  if (entity.indexOf("xyz") > -1)
  {
    url = "http://ares.elasticbeanstalk.com/api/products/getAllProducts";
  }
  else if (entity.indexOf("baby") > -1)
  {
    url = "http://ares.elasticbeanstalk.com/api/products/getTop4InCategories";
    gridLayout.style.opacity = 0.25;
  }

  xmlReq.onreadystatechange = function() {
    if (xmlReq.readyState == 4 && xmlReq.status == 200) {
        var myArr = JSON.parse(xmlReq.responseText);
        flag = true;
        var i = 0;
        var out = "";
        var banner = document.getElementById('bannerImg');
        if (entity.indexOf("xyz") > -1)
        {
          for (i=0;i<myArr.length;i++)
          {
            out += '<div id="catItem'+i+'" class="category" data-category="'+myArr[i]['CATEGORY']+
            ' data-subcategory="'+myArr[i]['SUBCATEGORY']+'">'+
            '<a><img src="'+myArr[i]['PRODUCTURL']+'" width="161" height="152"><span>'+myArr[i]['TITLE']+'</span></a>'+
            '<div class="itemToCart" onclick="assistCall(\'catItem'+i+'\')">'+
            '<img src="img/cart.png"></div></div>';
          }
          if (user["name"] === "Lisa")
            banner.src = 'img/baby3.png';
        }
        else if (entity.indexOf("baby") > -1) {
          for (i=0;i<myArr.length;i++)
          {
            if (i%4 === 0)
            {
              if (i > 0)
                out += '</div>';
              out += '<div ';
              if (myArr[i]['CATEGORY'] === "Essentials, food , 0-3 months")
              {
                out += 'onmouseover="addContent(\'babyMilk\')"';
              }
              out += '><div class="categoryHeading"><span>'+myArr[i]['CATEGORY']+'</span></div>';
            }
            out += '<div id="catItem'+i+'" class="category" data-category="'+myArr[i]['CATEGORY']+
            ' data-subcategory="'+myArr[i]['SUBCATEGORY']+'">'+
            '<a><img src="'+myArr[i]['PRODUCTURL']+'" width="161" height="152"><span>'+myArr[i]['TITLE']+'</span></a>'+
            '<div class="itemToCart" onclick="assistCall(\'catItem'+i+'\')">'+
            '<img src="img/cart.png"></div></div>';
            if (i === myArr.length - 1)
              out += '</div>';
          }
          if (entity.indexOf("baby0") > -1)
            banner.src = 'img/baby3.png';
          else if (entity.indexOf("baby18") > -1)
            banner.src = 'img/baby18.png';
          else if (entity.indexOf("babyGirl") > -1)
            banner.src = 'img/girl.png';
        }
        gridLayout.innerHTML = out;
        gridLayout.style.opacity = 1;
    }
  }
  if (entity.indexOf("xyz") > -1)
  {
    xmlReq.open("GET", url, true);
    xmlReq.send();
  }
  else if (entity.indexOf("baby") > -1)
  {
    var data = null;
    if (entity.indexOf("baby0") > -1)
      data = JSON.stringify([{"CATID":"Essentials, Cloths , 0-3 months"},{"CATID":"Essentials, food , 0-3 months"},{"CATID":"Essentials, diapers , 0-3 months"}]);
    else if (entity.indexOf("baby18") > -1)
      data = JSON.stringify([{"CATID":"Essentials, Cloths , 18-24 months"},{"CATID":"Essentials, Toys, 18-24 months"}]);
    else if (entity.indexOf("babyGirl") > -1)
      data = JSON.stringify([{"CATID":"Essentials, Girls Kids Clothing "},{"CATID":"Essentials, Girls Kids Toys "},{"CATID":"Essentials, Girls Kids Food "}]);

    xmlReq.open("POST", url, true);
    xmlReq.setRequestHeader("Content-Type", "application/json");
    xmlReq.send(data);
  }
}

function assistCall(id)
{
  var prod = document.getElementById(id);
  var img = prod.childNodes[1].childNodes[0];
  img.src = "img/cartSel.png";
  var topPos = window.scrollY + (window.innerHeight)/2 - 325;
  showPopUp('assistantPopup');
  var leftPos = (window.innerWidth)/2 - ((document.getElementById("assistantPopup")).clientWidth)/2;
  document.getElementById('assistantPopup').style.top = topPos.toString()+"px";
  document.getElementById('assistantPopup').style.left = leftPos.toString()+"px";
}
