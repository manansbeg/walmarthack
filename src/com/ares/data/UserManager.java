package com.ares.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.ares.util.ConnectionManager;

public class UserManager {
	Connection con =null;
	
	public UserManager() throws ClassNotFoundException, SQLException {
	
		con=	ConnectionManager.getConnection();
	}
	
	public String register(JSONObject user ) throws SQLException, JSONException{
		System.out.println("UserManager:register");
		String insertUser="INSERT INTO  \"AR_CUSTOMERS\" \r\n" + 
				"   (FIRSTNAME,\r\n" + 
				"	LASTNAME, \r\n" + 
				"	EMAIL, \r\n" + 
				"	ADDRESS,\r\n" + 
				"	CITY,\r\n" + 
				"	DOB,\r\n" + 
				"	GENDER\r\n" + 
				"	)\r\n" + 
				"	VALUES(?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = con.prepareStatement(insertUser);
		System.out.println(user.toString());
	    
		preparedStatement.setString(1,user.getString("FIRSTNAME") );
		preparedStatement.setString(2, user.getString("LASTNAME") );
		preparedStatement.setString(3,user.getString("EMAIL")  );
		preparedStatement.setString(4,user.getString("ADDRESS")  );
		preparedStatement.setString(5, user.getString("CITY") );
		preparedStatement.setString(6,user.getString("DOB")  );
		preparedStatement.setString(7, user.getString("GENDER") );
		preparedStatement.executeUpdate();
		preparedStatement.close();
		
		JSONObject response =new JSONObject();
		response.put("STATUS", "Success");
		response.put("EMAIL",user.getString("EMAIL") );
		System.out.println("done with insert for " + response.toString());
		
		return response.toString();
	}
	
	public String login(JSONObject user) throws JSONException, SQLException{
		System.out.println("UserManager:login");
		String email=null;
		String status=null;
		String selectSQL = "SELECT email FROM AR_CUSTOMERS WHERE EMAIL = ?";
		PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
		preparedStatement.setString(1,user.getString("EMAIL")  );
		ResultSet rs = preparedStatement.executeQuery( );
		while (rs.next()) {
			 email = rs.getString("EMAIL");
			
		}
		preparedStatement.close();
		if (email!=null)
		{
			status = "success";
		}
		else status ="fail";
		
		JSONObject response =new JSONObject();
		
		
		
		response.put("STATUS",status);
		response.put("EMAIL",user.getString("EMAIL") );
		System.out.println( "Login Status : "+response.toString());
		return response.toString();
		
		
	}

	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, JSONException {
		
		
		UserManager userManager =  new UserManager();
		
		JSONObject registerUserObject = new JSONObject();
		registerUserObject.put("FIRSTNAME","manan");
		registerUserObject.put("LASTNAME","beg");
		registerUserObject.put("EMAIL","manansbeg@gmail.com");
		registerUserObject.put("ADDRESS","12/9 trikuta Nagar");
		registerUserObject.put("CITY","Jammu");
		registerUserObject.put("DOB","20/03/1987");
		registerUserObject.put("GENDER","male");
		userManager.register(registerUserObject);
		
		
		JSONObject loginObject = new JSONObject();
		loginObject.put("EMAIL","manansbeg@gmail.com");
		System.out.println(userManager.login(loginObject));
		
	}
}
