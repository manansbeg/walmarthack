package com.ares.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ares.util.ConnectionManager;

public class ProductManager {
	Connection con =null;
	
	public ProductManager() throws ClassNotFoundException, SQLException {
	
		con=	ConnectionManager.getConnection();
	}
	
	public String getCategoryDetails(JSONObject category ) throws SQLException, JSONException{
		System.out.println("ProductManager:getCategoryDetails");
		
		
		String selectSQL = "select * from AR_CATEGORY where category =? or SUBCATEGORY = ?";
		PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
		preparedStatement.setString(1,category.getString("CATEGORY")  );
		preparedStatement.setString(2,category.getString("SUBCATEGORY")  );
		ResultSet rs = preparedStatement.executeQuery( );
		
		JSONObject response =new JSONObject();
		
		while (rs.next()) {
			response.put("CATID",rs.getString("CATID"));
			response.put( "CATEGORY",rs.getString("CATEGORY"));
			response.put("SUBCATEGORY", rs.getString("SUBCATEGORY"));
			response.put( "URL1", rs.getString("URL1"));
			response.put( "URL2", rs.getString("URL2"));
			response.put( "URL3", rs.getString("URL3"));
			response.put( "URL4", rs.getString("URL4"));
			response.put("URL5",  rs.getString("URL5"));
			
		}
		preparedStatement.close();
	
		
		
		
	
		return response.toString();
		
		
	}
	
	public String getProductsInACategory(JSONObject category ) throws SQLException, JSONException{
		System.out.println("ProductManager:getCategoryDetails");
		
		
		String selectSQL = "select * from AR_PRODUCTS products INNER JOIN  AR_CATEGORY  categories on products.catid=categories.catid where products.catid=?";
		PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
		preparedStatement.setString(1,category.getString("CATID")  );
		ResultSet rs = preparedStatement.executeQuery( );
		
		JSONArray products=new JSONArray();
		while (rs.next()) {
			JSONObject response =new JSONObject();
			response.put("CATID",rs.getString("CATID"));
			response.put( "CATEGORY",rs.getString("CATEGORY"));
			response.put("SUBCATEGORY", rs.getString("SUBCATEGORY"));
			response.put( "URL1", rs.getString("URL1"));
			response.put( "URL2", rs.getString("URL2"));
			response.put( "URL3", rs.getString("URL3"));
			response.put( "URL4", rs.getString("URL4"));
			response.put("URL5",  rs.getString("URL5"));
			response.put("SKU",  rs.getString("SKU"));
			response.put("DESCRIPTION",  rs.getString("DESCRIPTION"));
			response.put("TITLE",  rs.getString("TITLE"));
			response.put("PRODUCTURL",  rs.getString("PRODUCTURL"));
			products.put(response);
			
		}
		preparedStatement.close();
	
		
		
		
	
		return products.toString();
		
		
	}
	
	public String getProductDetails(JSONObject product ) throws SQLException, JSONException{
		System.out.println("ProductManager:getProductDetails");
		
		
		String selectSQL = "select * from AR_PRODUCTS products INNER JOIN  AR_CATEGORY  categories on products.catid=categories.catid where sku =? or title = ?";
		PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
		preparedStatement.setString(1,product.getString("SKU")  );
		preparedStatement.setString(2,product.getString("TITLE")  );
		ResultSet rs = preparedStatement.executeQuery( );
		
		
		JSONArray products=new JSONArray();
		while (rs.next()) {
			JSONObject response =new JSONObject();
			response.put("CATID",rs.getString("CATID"));
			response.put( "CATEGORY",rs.getString("CATEGORY"));
			response.put("SUBCATEGORY", rs.getString("SUBCATEGORY"));
			response.put( "URL1", rs.getString("URL1"));
			response.put( "URL2", rs.getString("URL2"));
			response.put( "URL3", rs.getString("URL3"));
			response.put( "URL4", rs.getString("URL4"));
			response.put("URL5",  rs.getString("URL5"));
			response.put("SKU",  rs.getString("SKU"));
			response.put("DESCRIPTION",  rs.getString("DESCRIPTION"));
			response.put("TITLE",  rs.getString("TITLE"));
			response.put("PRODUCTURL",  rs.getString("PRODUCTURL"));
			products.put(response);
			
		}
		preparedStatement.close();
	
		
		
		
	
		return products.toString();
		
		
	}
	
	
	public String getTop4InCategories(JSONArray array , int rownum) throws JSONException, SQLException{
		
		JSONArray products=new JSONArray();
		for(int i = 0 ;  i< array.length() ; i++)
		{
			String selectSQL = "select * from AR_PRODUCTS products LEFT JOIN  AR_CATEGORY  categories on products.catid=categories.catid where products.catid = ? AND rownum < ?";
			JSONObject object = (JSONObject) array.get(i);
			String category=(String) object.get("CATID");
			PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
			preparedStatement.setString(1, category);
			preparedStatement.setInt(2, rownum);
			
			ResultSet rs = preparedStatement.executeQuery( );
			while (rs.next()) {
				JSONObject response =new JSONObject();
				
				response.put("CATID",rs.getString("CATID"));
				response.put( "CATEGORY",rs.getString("CATEGORY"));
				response.put("SUBCATEGORY", rs.getString("SUBCATEGORY"));
				response.put( "URL1", rs.getString("URL1"));
				response.put( "URL2", rs.getString("URL2"));
				response.put( "URL3", rs.getString("URL3"));
				response.put( "URL4", rs.getString("URL4"));
				response.put("URL5",  rs.getString("URL5"));
				response.put("SKU",  rs.getString("SKU"));
				response.put("DESCRIPTION",  rs.getString("DESCRIPTION"));
				response.put("TITLE",  rs.getString("TITLE"));
				response.put("PRODUCTURL",  rs.getString("PRODUCTURL"));
				products.put(response);
				
			}
			
			
			
		}
		
		
		return products.toString();
	}
	public String getAllProducts() throws SQLException, JSONException{
		System.out.println("ProductManager:getProductDetails");
		
		
		String selectSQL = "select * from AR_PRODUCTS products LEFT JOIN  AR_CATEGORY  categories on products.catid=categories.catid";
		PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
	
		ResultSet rs = preparedStatement.executeQuery( );
		JSONArray array=new JSONArray();
		
		
		while (rs.next()) {
			JSONObject response =new JSONObject();
			
			response.put("CATID",rs.getString("CATID"));
			response.put( "CATEGORY",rs.getString("CATEGORY"));
			response.put("SUBCATEGORY", rs.getString("SUBCATEGORY"));
			response.put( "URL1", rs.getString("URL1"));
			response.put( "URL2", rs.getString("URL2"));
			response.put( "URL3", rs.getString("URL3"));
			response.put( "URL4", rs.getString("URL4"));
			response.put("URL5",  rs.getString("URL5"));
			response.put("SKU",  rs.getString("SKU"));
			response.put("DESCRIPTION",  rs.getString("DESCRIPTION"));
			response.put("TITLE",  rs.getString("TITLE"));
			response.put("PRODUCTURL",  rs.getString("PRODUCTURL"));
			array.put(response);
			
		}
		preparedStatement.close();
	
		
		
		
	
		return array.toString();
		
	}
	
	
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, JSONException {
		
		
		ProductManager productManager =  new ProductManager();
		
		JSONObject category = new JSONObject();
		category.put("CATEGORY","Baby And Kids");
		category.put("SUBCATEGORY","Baby Food");
		System.out.println(productManager.getCategoryDetails(category));
		
		
	
	//	System.out.println(userManager.login(loginObject));
		
	}
}
