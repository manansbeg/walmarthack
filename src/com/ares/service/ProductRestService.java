package com.ares.service;

 
/**
 * @author manan beg
 * 
 */
 

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ares.data.ProductManager;
import com.ares.data.UserManager;
import com.ares.util.Utility;
 
@Path("/products")
public class ProductRestService {
	
	ProductManager productManager= null;
	
	public ProductRestService() throws ClassNotFoundException, SQLException {
		productManager=new ProductManager();
	}
	
	
	@POST
	@Path("/getCatDetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getCatDetails(InputStream incomingData) throws JSONException, SQLException {
		
		
		
		JSONObject jsonObject = new JSONObject(Utility.inputStreamToString(incomingData));
		
		return Response.status(200).entity(productManager.getCategoryDetails(jsonObject).toString()).build();
	}
	
	@POST
	@Path("/getProductDetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getProductDetails(InputStream incomingData) throws JSONException, SQLException {
		
		
		
		JSONObject jsonObject = new JSONObject(Utility.inputStreamToString(incomingData));
		
		return Response.status(200).entity(productManager.getProductDetails(jsonObject).toString()).build();
	}
	
	
	
	@POST
	@Path("/getProductsInACategory")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getProductsInACategory(InputStream incomingData) throws JSONException, SQLException {
		
		
		
		JSONObject jsonObject = new JSONObject(Utility.inputStreamToString(incomingData));
		
		return Response.status(200).entity(productManager.getProductsInACategory(jsonObject).toString()).build();
	}
	
	
	@POST
	@Path("/getTop4InCategories")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getTop4InCategories(InputStream incomingData) throws JSONException, SQLException {
		
		
		
		JSONArray array = new JSONArray(Utility.inputStreamToString(incomingData));
		
		return Response.status(200).entity(productManager.getTop4InCategories(array, 5).toString()).build();
	}
	
	@GET
	@Path("/getAllProducts")
	
	@Produces(MediaType.TEXT_PLAIN)
	public Response getAllProducts() throws JSONException, SQLException {
		
		
		
	
		
		return Response.status(200).entity(productManager.getAllProducts().toString()).build();
	}
	
	
 
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "Product Rest Service is up";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
 
}