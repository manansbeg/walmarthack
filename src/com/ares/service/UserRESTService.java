package com.ares.service;

 
/**
 * @author manan beg
 * 
 */
 

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.ares.data.UserManager;
import com.ares.util.Utility;
 
@Path("/user")
public class UserRESTService {
	
	UserManager userManager= null;
	
	public UserRESTService() throws ClassNotFoundException, SQLException {
		userManager=new UserManager();
	}
	
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response register(InputStream incomingData) throws JSONException, SQLException {
		
		
		
		JSONObject jsonObject = new JSONObject(Utility.inputStreamToString(incomingData));
		
		return Response.status(200).entity(userManager.register(jsonObject).toString()).build();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response login(InputStream incomingData) throws JSONException, SQLException {
	
	
		
		JSONObject jsonObject = new JSONObject(Utility.inputStreamToString(incomingData));
		
		// return HTTP response 200 in case of success
		return Response.status(200).entity(userManager.login(jsonObject).toString()).build();
	}
	
	
 
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "User Service is up";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
 
}